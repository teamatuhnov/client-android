import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';

export default class PlayGround extends Component {
  constructor(props) {
    super(props)
    const { socket } = props

    this.state = {
      cardList: ['008', '209', '305', '306', '004', '312', '007', '212', '304', '309', '010', '307'],
      cardImages: {
          '004': require('../Images/004.png'),
          '005': require('../Images/005.png'),
          '006': require('../Images/006.png'),
          '007': require('../Images/007.png'),
          '008': require('../Images/008.png'),
          '009': require('../Images/009.png'),
          '010': require('../Images/010.png'),
          '011': require('../Images/011.png'),
          '012': require('../Images/012.png'),
          '204': require('../Images/204.png'),
          '205': require('../Images/205.png'),
          '206': require('../Images/206.png'),
          '207': require('../Images/207.png'),
          '208': require('../Images/208.png'),
          '209': require('../Images/209.png'),
          '210': require('../Images/210.png'),
          '211': require('../Images/211.png'),
          '212': require('../Images/212.png'),
          '304': require('../Images/304.png'),
          '305': require('../Images/305.png'),
          '306': require('../Images/306.png'),
          '307': require('../Images/307.png'),
          '308': require('../Images/308.png'),
          '309': require('../Images/309.png'),
          '310': require('../Images/310.png'),
          '311': require('../Images/311.png'),
          '312': require('../Images/312.png')
      },
      status: 'defence',
      active: false
    }
  }

  chooseCard(card) {
    console.log(card);
  }

  render() {
    const images = this.state.cardImages;
    const marginCard = -((this.state.cardList.length - 6) * 8)
    const cards = this.state.cardList.map(function(card, index) {
      const margin = index ? {marginLeft: marginCard} : {};
      return (<TouchableOpacity key={card} style={[style.card, margin]} onPress={this.chooseCard.bind(this, card)}>
                <Image
                    source={images[card]}
                    style={style.card}
                    resizeMode={Image.resizeMode.contain}
                />
              </TouchableOpacity>);
    }.bind(this));
    let statusIcon = null;


    switch (this.state.status) {
      case 'attack':
        statusIcon = require('../Images/turn-attack.png');
      break;
      case 'defence':
        statusIcon = require('../Images/turn-defence.png');
      break;
    }

    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <Image source={require('../Images/table-bg.png')} style={style.bgImage} resizeMode={Image.resizeMode.contain}>
          <View style={style.info}>
            {statusIcon && <Image source={statusIcon} style={style.statusIcon}/>}
          </View>
          <View style={style.cards}>
            {cards}
          </View>
        </Image>
      </View>
    );
  }
}

const style=StyleSheet.create({
  bgImage: {
    flex: 1,
    justifyContent: 'center',
    width: null,
    height: null,
  },
  info: {
    flex: 0.5,
    flexDirection: 'row',
    paddingLeft: 5,
    paddingRight: 5,
  },
  cards: {
    flex: 0.5,
    flexDirection: 'row',
    paddingLeft: 5,
    paddingRight: 5,
  },
  card: {
    width: 90,
    height: 150
  },
  statusIcon:{
    marginTop: 20,
    marginLeft: 20
  }
})
