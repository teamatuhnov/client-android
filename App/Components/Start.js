import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableHighlight,
  Image, StyleSheet
} from 'react-native';
import PlayGround from './PlayGround';

const io = require('socket.io-client/socket.io');

export default class Start extends Component {
  constructor(props) {
    super(props)

    this.state = {
      name: '',
      server: '',
      error: ''
    }
  }

  connectToTable() {
    const { name,  server } = this.state;

    this.props.navigator.replace({name: 'PlayGround', component: PlayGround, passProps: {socket: socket, name: name}});
    return;

    if (!name || !server) {
      this.setState({error: 'Заполните все поля'})
      return;
    }

    const socket = io(`http://${this.state.server}:3000`, {
      transports: ['websocket']
    });

    socket.on('connect', ()=>{
      this.props.navigator.push({name: 'PlayGround', component: PlayGround, passProps: {socket: socket, name: name}});
    });

    socket.on('connect_error', ()=>{
      this.setState({error: 'Ошибка подключения, проверьте адрес сервер и соединение с сетью'});
      socket.disconnect();
    })
  }

  render() {

    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <Image source={require('../Images/table-bg.png')} style={style.bgImage} resizeMode={Image.resizeMode.contain}>
          <View style={style.inputArea}>
            <Text style={style.label}>
              Имя в игре:
            </Text>
            <View style={style.inputWrapper}>
              <TextInput
                underlineColorAndroid='#FFFFFF'
                autoCapitalize='sentences'
                onChangeText={(name) => this.setState({name, error: ''})}
                value={this.state.name}>
              </TextInput>
            </View>
            <Text style={style.label}>
              IP-адрес стола:
            </Text>
            <View style={style.inputWrapper}>
              <TextInput
                underlineColorAndroid='#FFFFFF'
                autoCapitalize='sentences'
                onChangeText={(server) => this.setState({server, error: ''})}
                value={this.state.server}>
              </TextInput>
            </View>
            <Text style={style.error}>
              {this.state.error}
            </Text>
            <TouchableHighlight onPress={this.connectToTable.bind(this)} style={style.button}>
              <Text style={style.button.text}>ВСТУПИТЬ В ИГРУ</Text>
            </TouchableHighlight>
          </View>
        </Image>
      </View>
    );
  }
}
const style = StyleSheet.create({
  bgImage: {
    flex: 1,
    justifyContent: 'center',
    width: null,
    height: null,
  },
  label: {
    color: '#FFF',
    fontSize: 18
  },
  inputArea: {
    width: 400,
    alignSelf: 'center'
  },
  input: {
    borderWidth: 0,
    fontSize: 12,
  },
  inputWrapper: {
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    marginBottom: 20
  },
  button: {
    alignSelf: 'flex-end',
    borderRadius: 5,
    padding: 5,
    paddingRight: 10,
    paddingLeft: 10,
    backgroundColor: '#A8A8A8'
  },
  buttonText: {
    color: '#707070'
  },
  error: {
    color: 'red',
    fontSize: 14,
    textAlign: 'center',
    marginTop: -10,
    marginBottom: 10,
    fontWeight: 'bold',
  }
})
