import React, { Component } from 'react';
import {
  AppRegistry,
  Navigator
} from 'react-native';
import Start from './App/Components/Start';
import Orientation from 'react-native-orientation'

window.navigator.userAgent = "react-native";

Orientation.lockToLandscape();

class tvpocker extends Component {
  _renderScene(route, navigator) {
    if (route.component) {
        return <route.component navigator={navigator} {...route.passProps}/>;
    }
  }

  render() {
    return (
      <Navigator
        initialRoute={{name: 'Start', component: Start}}
        configureScene={() => {
            return Navigator.SceneConfigs.FadeAndroid;
        }}
        renderScene={this._renderScene}
        sceneStyle={{backgroundColor: '#10555C'}}
      />
    );
  }
}

AppRegistry.registerComponent('tvpocker', () => tvpocker);
